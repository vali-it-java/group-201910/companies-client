
// Functions for communicating with backend-API here...

async function getCompanies() {
    let result = await fetch(`${API_URL}/companies`);
    let companies = await result.json();
    return companies;
}

async function getCompany(id) {
    let result = await fetch(`${API_URL}/companies/${id}`);
    let company = await result.json();
    return company;
}

async function deleteCompany(id) {
    await fetch(`${API_URL}/companies/${id}`, {
        method: 'DELETE'
    });
}

async function postCompany(company) {
    await fetch(`${API_URL}/companies`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(company)
    });
}

async function putCompany(company) {
    await fetch(`${API_URL}/companies`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(company)
    });
}

function fetchSomething() {
    return fetch(
        `${API_URL}/something`,
        {
            method: 'GET'
        }
    )
    .then(something => something.json());
}

function postSomething(something) {
    return fetch(
        `${API_URL}/something`, 
        {
            method: 'POST', 
            body: JSON.stringify(something)
        }
    );
}

function deleteSomething(id) {
    return fetch(
        `${API_URL}/something/${id}`,
        {
            method: 'DELETE'
        }
    );
}
