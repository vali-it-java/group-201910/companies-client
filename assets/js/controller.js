
// Controller functions here...

async function loadCompanies() {
    let companies = await getCompanies();

    let companiesHtml = '';

    for (let i = 0; i < companies.length; i++) {
        companiesHtml = companiesHtml + `
            <tr>
                <td>${companies[i].id}</td>
                <td>${companies[i].name}</td>
                <td><img src="${companies[i].logo}" height="50"></td>
                <td>${companies[i].established}</td>
                <td>${companies[i].employees}</td>
                <td>
                    <button type="button" class="btn btn-primary" onclick="handleCompanyUpdateModalOpening(${companies[i].id})">Muuda</button>
                    <button class="btn btn-danger" onclick="handleCompanyDelete(${companies[i].id})">Kustuta</button>
                </td>
            </tr>
        `;
    }

    document.querySelector('#companiesList').innerHTML = companiesHtml;
}

async function handleCompanyDelete(id) {
    if (confirm('Oled sa ikka kindel, et tead, mida teed?')) {
        await deleteCompany(id);
        loadCompanies();
    }
}

function handleCompanyAddModalOpening() {
    cleanCompanyFormFields();
    $('#companyModal').modal('show');
}

async function handleCompanyUpdateModalOpening(id) {
    cleanCompanyFormFields();
    let company = await getCompany(id);
    if(company) {
        document.querySelector('#companyId').value = company.id;
        document.querySelector('#name').value = company.name;
        document.querySelector('#logo').value = company.logo;
        document.querySelector('#established').value = company.established;
        document.querySelector('#employees').value = company.employees;
        $('#companyModal').modal('show');
    }
}

async function handleSave(){

    if (!validateInput()) {
        return;
    }

    if(parseInt(document.querySelector('#companyId').value) > 0) {
        // update operatsioon
        let company = {
            id: document.querySelector('#companyId').value,
            name: document.querySelector('#name').value,
            logo: document.querySelector('#logo').value,
            established: document.querySelector('#established').value,
            employees: parseInt(document.querySelector('#employees').value)
        };
        await putCompany(company);
    } else {
        // add operatsioon
        let company = {
            name: document.querySelector('#name').value,
            logo: document.querySelector('#logo').value,
            established: document.querySelector('#established').value,
            employees: parseInt(document.querySelector('#employees').value)
        };
        await postCompany(company);
    }

    $('#companyModal').modal('hide');
    loadCompanies();
}

function cleanCompanyFormFields() {
    document.querySelector('#companyId').value = '';
    document.querySelector('#name').value = '';
    document.querySelector('#logo').value = '';
    document.querySelector('#established').value = '';
    document.querySelector('#employees').value = '';
    hideError();
}

function displayError(errorMessage) {
    let errorBox = document.querySelector('#errorBox');
    errorBox.style.display = 'block';
    errorBox.innerText = errorMessage;   
}

function hideError() {
    let errorBox = document.querySelector('#errorBox');
    errorBox.style.display = 'none';
    errorBox.innerText = '';  
}

function validateInput() {
    let name = document.querySelector('#name').value;
    let logo = document.querySelector('#logo').value;
    let established =  document.querySelector('#established').value;
    let employees =  document.querySelector('#employees').value;
    
    if (name == null || name.length < 2) {
        displayError('Nimi puudu või liiga lühike (min 2 tähte)!');
        return false;
    }

    if (logo == null || logo.length < 1) {
        displayError('Logo on puudu!');
        return false;
    }

    if (!logo.startsWith("http://") && !logo.startsWith("https://")) {
        displayError('Logo protokoll vale. Lubatud vaid http:// ja https://!');
        return false;
    }

    if (established == null || established.length < 1) {
        displayError('Asutamiskuupäev puudu!');
        return false;
    }

    let establishedDate = new Date(established);
    let currentDate = new Date();
    if (currentDate.getTime() < establishedDate.getTime()) {
        displayError('Asutamiskuupäev peab olema minevikus!');
        return false;
    }

    if (!(parseInt(employees) > 0) || parseInt(employees) != employees || employees.length > 5) {
        displayError('Töötajaid peab olema vähemalt üks! Lubatud vaid täisarvud!');
        return false;
    }

    return true;
}